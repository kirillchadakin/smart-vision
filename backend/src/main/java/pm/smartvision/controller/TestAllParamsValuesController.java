package pm.smartvision.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pm.smartvision.service.SampleParamValuesService;

@Tag(name = "Для тестов параметров (общий)", description = "Эндпоинты для просмотра и добавления тестовых значений параметров")
@RestController
@RequestMapping("/api/params/values")
@RequiredArgsConstructor
public class TestAllParamsValuesController {

    private final SampleParamValuesService sampleParamValuesService;

    @Operation(
            summary = "Остановка генерации тестовых значений",
            description = "Останавливает все генерации значений не дожидаясь конца таймаута."
    )
    @GetMapping("/stop-test-generation")
    public void stopTestGeneration() {
        sampleParamValuesService.stopAllGenerations();
    }

}

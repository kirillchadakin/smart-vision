package pm.smartvision.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pm.smartvision.dto.api.*;
import pm.smartvision.service.ParamService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;

@Tag(name = "Графики", description = "Эндпоинты для посторения графиков")
@RestController
@RequestMapping("/api/params/graph")
@RequiredArgsConstructor
public class ParamsGraphsController {

    private final ParamService paramService;

    @Operation(
            summary = "Построение кругового графика",
            description = "Круговой график содержит сумму всех значений за заданный период по каждому параметру. " +
                    "Список идентификаторов параметров можно не передавать, тогда график будет строиться по всем " +
                    "параметрам, по которым есть данные.",
            responses = @ApiResponse(
                    responseCode = "200",
                    description = "OK. Ответ содержит ассоциативный массив, " +
                            "ключи которого - идентификаторы параметров, " +
                            "а значения - сумма за указанный период по этому параметру.",
                    content = @Content(
                            examples = @ExampleObject(
                                    value = """
                                            {
                                                1: 3,
                                                2: 10
                                            }
                                            """
                            )
                    )
            )
    )
    @PostMapping("/circle")
    public Mono<Map<Long, Long>> circle(@RequestBody ParamCircleGraphRequest request) {
        return paramService.circleGraph(request.paramIds(), request.startTime(), request.endTime());
    }

    @GetMapping(path = "/circle/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map<Long, Long>> circleSse(ParamCircleGraphSseRequest request) {
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(request.period()))
                .flatMap(sequence -> {
                    Instant now = Instant.now();
                    return paramService.circleGraph(request.paramIds(),
                            now.minus(Duration.ofMinutes(request.depth())), now);
                });
    }

    @Operation(
            summary = "Построение гистограммы (столбчатой диаграммы)",
            description = "Гистограмма содержит ",
            responses = @ApiResponse(
                    responseCode = "200",
                    description = "OK. Ответ содержит ассоциативный массив, " +
                            "ключи которого - интервал, за который производитлось суммирование, " +
                            "в формате \"${startTime},${endTime}\", " +
                            "а значения - сумма за указанный период.",
                    content = @Content(
                            examples = @ExampleObject(
                                    value = """
                                            {
                                                "2024-02-29T21:25:32.665Z,2024-02-29T21:40:32.665Z": 3,
                                                "2024-02-29T21:40:32.665Z,2024-02-29T21:55:32.665Z": 10
                                            }
                                            """
                            )
                    )
            )
    )
    @PostMapping("/hist")
    public Mono<Map<String, Long>> hist(@RequestBody ParamHistGraphRequest request) {
        return paramService.histGraph(request.paramId(), request.bandAmount(), request.startTime(), request.endTime());
    }

    @GetMapping(path = "/hist/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map<String, Long>> histSse(ParamHistGraphSseRequest request) {
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(request.period()))
                .flatMap(sequence -> {
                    Instant now = Instant.now();
                    return paramService.histGraph(request.paramId(), request.bandAmount(),
                            now.minus(Duration.ofMinutes(request.depth())), now);
                });
    }



    @Operation(
            summary = "Построение линейного графика",
            responses = @ApiResponse(
                    responseCode = "200",
                    description = "OK. Ответ содержит ассоциативный массив, " +
                            "ключи которого - интервал, за который производитлось суммирование, " +
                            "в формате \"${startTime},${endTime}\", " +
                            "а значения - сумма за указанный период.",
                    content = @Content(
                            examples = @ExampleObject(
                                    value = """
                                            {
                                                "2024-02-29T21:25:32.665Z,2024-02-29T21:40:32.665Z": 3,
                                                "2024-02-29T21:40:32.665Z,2024-02-29T21:55:32.665Z": 10
                                            }
                                            """
                            )
                    )
            )
    )
    @PostMapping("/line")
    public Mono<Map<Instant, Integer>> line(@RequestBody ParamLineGraphRequest request) {
        return paramService.lineGraph(request.paramId(), request.startTime(), request.endTime());
    }

    @GetMapping(path = "/line/sse", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Map<Instant, Integer>> lineSse(ParamLineGraphSseRequest request) {
        return Flux.interval(Duration.ZERO, Duration.ofSeconds(request.period()))
                .flatMap(sequence -> {
                    Instant now = Instant.now();
                    return paramService.lineGraph(request.paramId(), now.minus(Duration.ofSeconds(request.depth())), now);
                });
    }
}

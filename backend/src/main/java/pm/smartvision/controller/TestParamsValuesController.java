package pm.smartvision.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.bind.DefaultValue;
import org.springframework.web.bind.annotation.*;
import pm.smartvision.model.ParamValue;
import pm.smartvision.service.ParamService;
import pm.smartvision.service.SampleParamValuesService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;

@Tag(name = "Для тестов параметров", description = "Эндпоинты для просмотра и добавления тестовых значений параметров")
@RestController
@RequestMapping("/api/params/{paramId}/values")
@RequiredArgsConstructor
public class TestParamsValuesController {

    private final ParamService paramService;
    private final SampleParamValuesService sampleParamValuesService;

    @GetMapping
    public Flux<ParamValue> find(@PathVariable Long paramId, @RequestParam Instant from, @RequestParam Instant to) {
        return paramService.findValues(paramId, from, to);
    }

    @DeleteMapping
    public Mono<Void> clearValues(@PathVariable Long paramId) {
        return paramService.clearParamValues(paramId);
    }

    @Operation(
            summary = "Запуск генерации тестовых значений",
            description = "Запусает генерацию значений для paramId с интервалом interval " +
                    "(по умолчанию 1 секунда) в течении timeout (по умолчанию 10 минут) " +
                    "случайных значений в диапазоне от min (по умолчанию 1) до " +
                    "max (по умолчанию 30)."
    )
    @GetMapping("/start-test-generation")
    public void startTestGeneration(
            @PathVariable Long paramId,
            @RequestParam(required = false) @DefaultValue("600000") long timeout,  // 10 минут
            @RequestParam(required = false) @DefaultValue("1000") long interval,
            @RequestParam(required = false) @DefaultValue("1") int min,
            @RequestParam(required = false) @DefaultValue("30") int max
    ) {
        sampleParamValuesService.startGenerate(paramId,
                Duration.ofMillis(interval), Duration.ofMillis(timeout), min, max);
    }

    @Operation(
            summary = "Остановка генерации тестовых значений",
            description = "Останавливает генерацию значений для paramId " +
                    "не дожидаясь конца таймаута."
    )
    @GetMapping("/stop-test-generation")
    public void stopTestGeneration(@PathVariable Long paramId) {
        sampleParamValuesService.stopGenerate(paramId);
    }

    @PostMapping
    public Mono<Void> addValue(@PathVariable Long paramId, @RequestBody ParamValue paramValue) {
        paramValue.setParamId(paramId);
        return paramService.addParamValue(paramValue);
    }

}

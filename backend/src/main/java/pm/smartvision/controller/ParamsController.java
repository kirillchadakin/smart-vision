package pm.smartvision.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pm.smartvision.dto.api.ParamStatusResponse;
import pm.smartvision.model.Param;
import pm.smartvision.service.ParamService;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Tag(name="Параметры", description="Эндпоинты для просмотра и настройки параметров")
@RestController
@RequestMapping("/api/params")
@RequiredArgsConstructor
public class ParamsController {

    private final ParamService paramService;

    @GetMapping
    public Flux<Param> params() {
        return paramService.findAll();
    }

    @PostMapping
    public Mono<Param> saveParam(@RequestBody Param param) {
        return paramService.save(param);
    }

    @PutMapping
    public Mono<Param> updateParam(@RequestBody Param param) {
        param.setStatus(true);
        return paramService.update(param);
    }

    @DeleteMapping("/{paramId}")
    public Mono<Param> delete(@PathVariable Long paramId) {
        return paramService.remove(paramId);
    }

    @GetMapping("/{paramId}/status")
    public Mono<ParamStatusResponse> paramStatus(@PathVariable Long paramId) {
        return paramService.getStatusById(paramId)
                .map(ParamStatusResponse::new);
    }


}

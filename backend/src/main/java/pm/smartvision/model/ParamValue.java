package pm.smartvision.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.time.Instant;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "create")
@Table("param_values")
public class ParamValue {
    @Schema(
            hidden = true
    )
    Long paramId;

    @NotNull
    @Column("created_at")
    Instant instant;

    @NotNull
    Integer value;
}

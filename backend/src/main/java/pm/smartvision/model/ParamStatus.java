package pm.smartvision.model;

public enum ParamStatus {
    OK, WARING, ERROR;

    public static ParamStatus valueOf(boolean b) {
        return b ? ERROR : OK;
    }
}

package pm.smartvision.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@Table("params")
public class Param {
    @Schema(
            description = "Идентификатор параметра",
            example = "1"
    )
    @Id
    Long id;

    @Schema(
            description = "Название параметра",
            example = "Ошибки 404"
    )
    String name;

    @Schema(
            description = "Пороговое значение, после которого сборщик отправляет событие",
            example = "20"
    )
    Integer threshold;

    @Schema(
            description = "Глубина анализа в минутах. При анализе возможных проблем " +
                    "суммируется количество произошедших за период от текущего времени минус количество depth минут.",
            example = "60"
    )
    Integer depth;

    @Schema(
            description = "Регулярное выражение, по которому выявляется лог события параметра. " +
                    "Также может быть кодом ошибки.",
            example = "404"
    )
    String pattern;

    @Schema(
            description = "Адрес сервера на котором находится сборщик этого параметра.",
            example = "localhost"
    )
    String collectorUrl;

    @Schema(
            accessMode = Schema.AccessMode.READ_ONLY,
            description = "Текущее состояние параметра.",
            example = "OK"
    )
    Boolean status;
}

package pm.smartvision.repository;

import lombok.RequiredArgsConstructor;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.stereotype.Repository;
import pm.smartvision.dto.db.CircleGraphDto;
import pm.smartvision.model.Param;
import pm.smartvision.model.ParamValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Collection;
import java.util.Objects;

import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.query;

@Repository
@RequiredArgsConstructor
public class ParamRepositoryImpl implements ParamRepository {

    private final R2dbcEntityTemplate template;

    @Override
    public Flux<Param> findAll() {
        return template.select(Param.class).all();
    }

    @Override
    public Mono<Boolean> existsById(Long paramId) {
        return template.select(Param.class)
                .matching(query(where("id").is(paramId)))
                .exists();
    }

    @Override
    public Mono<Param> save(Param param) {
        return template.insert(param);
    }

    @Override
    public Mono<Param> update(Param param) {
        return template.update(param);
    }

    @Override
    public Mono<Param> remove(Long paramId) {
        Param param = new Param();
        param.setId(paramId);
        return template.delete(param);
    }

    @Override
    public Mono<Boolean> getStatusById(Long paramId) {
        return template.select(Param.class)
                .matching(query(where("id").is(paramId)))
                .one()
                .map(Param::getStatus);
    }

    @Override
    public Flux<ParamValue> findValues(Long paramId, Instant from, Instant to) {
        return template.select(ParamValue.class)
                .matching(query(where("instant").between(from, to)))
                .all();
    }

    @Override
    public Mono<Void> addParamValue(ParamValue paramValue) {
        return template.insert(paramValue).then();
    }

    @Override
    public Mono<Void> clearParamValues(Long paramId) {
        Param param = new Param();
        param.setId(Objects.requireNonNull(paramId));
        return template.delete(ParamValue.class)
                .matching(query(where("param_id").is(paramId)))
                .all()
                .then();
    }

    @Override
    public Flux<CircleGraphDto> circleGraph(Collection<Long> paramIds, Instant start, Instant end) {
        if (Objects.requireNonNull(paramIds).isEmpty()) {
            return Flux.empty();
        }
        return template.getDatabaseClient().sql("""
                        with param_values_in_interval as (
                            select param_id, value
                            from param_values
                            where param_id in (:paramIds) and created_at between :start and :end
                        )
                        select param_id, sum(value) as valueSum
                        from param_values_in_interval
                        group by param_id
                        """
                )
                .bind("paramIds", paramIds)
                .bind("start", start)
                .bind("end", end)
                .fetch()
                .all()
                .map(row -> new CircleGraphDto((Long) row.get("param_id"), (Long) row.get("valueSum")));
    }

    @Override
    public Flux<ParamValue> paramValuesBetween(Long paramId, Instant start, Instant end) {
        return template.select(ParamValue.class)
                .matching(query(where("param_id").is(paramId)
                        .and("created_at").between(start, end))
                )
                .all();
    }
}

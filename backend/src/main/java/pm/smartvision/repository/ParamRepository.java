package pm.smartvision.repository;

import pm.smartvision.dto.db.CircleGraphDto;
import pm.smartvision.model.Param;
import pm.smartvision.model.ParamValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Collection;

public interface ParamRepository {
    Flux<Param> findAll();
    Mono<Boolean> existsById(Long paramId);
    Mono<Param> save(Param param);
    Mono<Param> update(Param param);
    Mono<Param> remove(Long paramId);
    Mono<Boolean> getStatusById(Long paramId);
    Flux<ParamValue> findValues(Long paramId, Instant from, Instant to);
    Mono<Void> addParamValue(ParamValue paramValue);
    Mono<Void> clearParamValues(Long paramId);
    Flux<CircleGraphDto> circleGraph(Collection<Long> paramIds, Instant start, Instant end);
    Flux<ParamValue> paramValuesBetween(Long paramId, Instant start, Instant end);
}

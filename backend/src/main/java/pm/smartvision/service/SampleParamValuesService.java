package pm.smartvision.service;

import org.springframework.stereotype.Service;
import pm.smartvision.model.ParamValue;
import pm.smartvision.repository.ParamRepository;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.time.Instant;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class SampleParamValuesService {
    private final ParamRepository paramRepository;
    private final Map<Long, Disposable> subscriptions = new ConcurrentHashMap<>();

    public SampleParamValuesService(ParamRepository paramRepository) {
        this.paramRepository = paramRepository;
    }

    public void startGenerate(Long paramId, Duration interval, Duration timeout, int min, int max) {
        Disposable subscribe = paramRepository.existsById(Objects.requireNonNull(paramId))
                .filter(isExists -> isExists)
                .flatMapMany(isExists -> Flux.interval(interval).take(timeout)
                        .flatMap(i -> paramRepository.addParamValue(
                                        ParamValue.create(paramId, Instant.now(), ThreadLocalRandom.current().nextInt(min, max))
                                )
                        )
                )
                .subscribe();
        subscriptions.put(paramId, subscribe);
    }

    public void stopGenerate(Long paramId) {
        Disposable subscription = subscriptions.get(Objects.requireNonNull(paramId));
        if (subscription != null) {
            subscription.dispose();
        }
    }

    public void stopAllGenerations() {
        subscriptions.values().forEach(Disposable::dispose);
        subscriptions.clear();
    }
}

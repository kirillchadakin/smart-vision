package pm.smartvision.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import pm.smartvision.model.ParamValue;
import pm.smartvision.repository.ParamRepository;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;

@Service
@RequiredArgsConstructor
@Log4j2
public class AnalyzeService {

    private final ParamRepository paramRepository;

    @Scheduled(fixedRate = 5000)
    public void analyze() {
        log.debug("Analyze started.");
        paramRepository.findAll()
                .flatMap(param -> {
                    Instant now = Instant.now();
                    return paramRepository.findValues(param.getId(), now.minus(Duration.ofMinutes(param.getDepth())), now)
                            .map(ParamValue::getValue)
                            .reduce(0L, Long::sum)
                            .flatMapMany(sum -> {
                                boolean status = sum > param.getThreshold();
                                if (param.getStatus() != status) {
                                    log.info("Status for param '{}' id={} is changed from {} to {}",
                                            param.getName(), param.getId(), param.getStatus(), status);
                                    param.setStatus(status);
                                    return paramRepository.update(param);
                                }
                                return Mono.empty();
                            });
                })
                .subscribe();
    }
}

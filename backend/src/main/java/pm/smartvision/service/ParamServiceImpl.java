package pm.smartvision.service;

import org.springframework.stereotype.Service;
import pm.smartvision.dto.db.CircleGraphDto;
import pm.smartvision.dto.db.HistGraphDto;
import pm.smartvision.model.Param;
import pm.smartvision.model.ParamValue;
import pm.smartvision.repository.ParamRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

@Service
public class ParamServiceImpl implements ParamService {
    private final ParamRepository paramRepository;

    public ParamServiceImpl(ParamRepository paramRepository) {
        this.paramRepository = paramRepository;
    }

    @Override
    public Flux<Param> findAll() {
        return paramRepository.findAll();
    }

    @Override
    public Mono<Param> save(Param param) {
        param.setStatus(false);
        return paramRepository.save(param);
    }

    @Override
    public Mono<Param> update(Param param) {
        return paramRepository.update(param);
    }

    @Override
    public Mono<Param> remove(Long paramId) {
        return paramRepository.remove(paramId);
    }

    @Override
    public Mono<Boolean> getStatusById(Long paramId) {
        return paramRepository.getStatusById(paramId);
    }

    @Override
    public Flux<ParamValue> findValues(Long paramId, Instant from, Instant to) {
        return paramRepository.findValues(paramId, from, to);
    }

    @Override
    public Mono<Void> addParamValue(ParamValue paramValue) {
        return paramRepository.addParamValue(paramValue);
    }

    @Override
    public Mono<Void> clearParamValues(Long paramId) {
        return paramRepository.clearParamValues(paramId);
    }

    @Override
    public Mono<Map<Long, Long>> circleGraph(Collection<Long> paramIds, Instant start, Instant end) {
        return paramRepository.circleGraph(paramIds, start, end)
                .collectMap(CircleGraphDto::paramId, CircleGraphDto::sum, TreeMap::new)
                // Если для периода [start, end) не найдены значения c paramId, устанавливаем для них 0
                .doOnNext(graphValues -> paramIds.forEach(paramId -> graphValues.putIfAbsent(paramId, 0L)));
    }

    @Override
    public Mono<Map<String, Long>> histGraph(Long paramId, int bandAmount, Instant start, Instant end) {
        Duration bandWidth = Duration.between(start, end).dividedBy(bandAmount);
        return paramRepository.paramValuesBetween(paramId, start,end)
                .collect(() -> new long[bandAmount], (bandSums, paramValue) -> {
                    int band = (int) Duration.between(start, paramValue.getInstant()).dividedBy(bandWidth);
                    bandSums[band] += paramValue.getValue();
                })
                .map(bandSums -> convertToHistGraphDtos(start, bandSums, bandWidth))
                .flatMapMany(Flux::fromArray)
                .collectMap(
                        histGraphDto -> String.format("%s,%s", histGraphDto.from(), histGraphDto.to()),
                        HistGraphDto::value,
                        TreeMap::new
                );
    }

    private static HistGraphDto[] convertToHistGraphDtos(Instant start, long[] bandSums, Duration bandWidth) {
        HistGraphDto[] histGraphDtos = new HistGraphDto[bandSums.length];
        for (int i = 0; i < bandSums.length; i++) {
            histGraphDtos[i] = new HistGraphDto(
                    start.plus(bandWidth.multipliedBy(i)),
                    start.plus(bandWidth.multipliedBy(i + 1)),
                    bandSums[i]
            );
        }
        return histGraphDtos;
    }

    @Override
    public Mono<Map<Instant, Integer>> lineGraph(Long paramId, Instant start, Instant end) {
        return paramRepository.findValues(paramId, start, end)
                .collectMap(ParamValue::getInstant, ParamValue::getValue, TreeMap::new);
    }
}

package pm.smartvision.service;

import pm.smartvision.model.Param;
import pm.smartvision.model.ParamValue;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;
import java.util.Collection;
import java.util.Map;

public interface ParamService {
    Flux<Param> findAll();
    Mono<Param> save(Param param);
    Mono<Param> update(Param param);
    Mono<Param> remove(Long paramId);
    Mono<Boolean> getStatusById(Long paramId);
    Flux<ParamValue> findValues(Long paramId, Instant from, Instant to);
    Mono<Void> addParamValue(ParamValue paramValue);
    Mono<Void> clearParamValues(Long paramId);
    Mono<Map<Long, Long>> circleGraph(Collection<Long> paramIds, Instant start, Instant end);
    Mono<Map<String, Long>> histGraph(Long paramId, int bandAmount, Instant start, Instant end);
    Mono<Map<Instant, Integer>> lineGraph(Long paramId, Instant start, Instant end);
}

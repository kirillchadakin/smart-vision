package pm.smartvision.dto.db;

public record CircleGraphDto(
        Long paramId,
        Long sum
) {
}

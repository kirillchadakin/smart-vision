package pm.smartvision.dto.db;

import java.time.Instant;

public record HistGraphDto(
        Instant from,
        Instant to,
        Long value
) {
}

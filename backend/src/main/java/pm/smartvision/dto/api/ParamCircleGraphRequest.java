package pm.smartvision.dto.api;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.Instant;
import java.util.List;

@Schema(description = "Запрос на построение круговой диаграммы")
public record ParamCircleGraphRequest(

        @Schema(
                description = "Список идентификаторов параметров. Если не задан, график строится для всех параметров.",
                requiredMode = Schema.RequiredMode.NOT_REQUIRED
        )
        List<Long> paramIds,

        @Schema(
                description = "Начало диапазона"
        )
        Instant startTime,

        @Schema(
                description = "Конец диапазона"
        )
        Instant endTime
) {

}

package pm.smartvision.dto.api;

import io.swagger.v3.oas.annotations.media.Schema;

public record ParamHistGraphSseRequest(
        @Schema(
                description = "ID параметра для которого будет строиться график.",
                example = "1"
        )
        Long paramId,

        @Schema(
                description = "Количество столбцов",
                example = "10"
        )
        Integer bandAmount,

        @Schema(
                description = "Период обновления графика в секундах.",
                example = "5"
        )
        Long period,

        @Schema(
                description = "Диапазон построения графика в минутах. События будут приходить в диапазоне [now - depth, now).",
                example = "60"
        )
        Long depth
        ) {
}

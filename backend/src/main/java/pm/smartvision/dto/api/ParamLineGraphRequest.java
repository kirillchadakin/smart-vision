package pm.smartvision.dto.api;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.Instant;

@Schema(
        description = "Запрос на построение линейного графика."
)
public record ParamLineGraphRequest(

        @Schema(
                description = "ID параметра для которого будет строиться график.",
                example = "1"
        )
        Long paramId,

        @Schema(
                description = "Начало диапазона"
        )
        Instant startTime,

        @Schema(
                description = "Конец диапазона"
        )
        Instant endTime
) {
}

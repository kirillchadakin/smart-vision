package pm.smartvision.dto.api;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

@Schema(description = "Запрос на построение круговой диаграммы")
public record ParamCircleGraphSseRequest(

        @Schema(
                description = "Список идентификаторов параметров. Если не задан, график строится для всех параметров.",
                requiredMode = Schema.RequiredMode.NOT_REQUIRED
        )
        List<Long> paramIds,

        @Schema(
                description = "Период обновления графика в секундах.",
                example = "5"
        )
        Long period,

        @Schema(
                description = "Диапазон построения графика в минутах. События будут приходить в диапазоне [now - depth, now).",
                example = "60"
        )
        Long depth
) {

}

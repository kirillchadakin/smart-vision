package pm.smartvision.dto.api;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.Instant;

@Schema(
        description = "Запрос на построение гистограммы."
)
public record ParamHistGraphRequest(

        @Schema(
                description = "ID параметра для которого будет строиться график.",
                example = "1"
        )
        Long paramId,

        @Schema(
                description = "Начало диапазона"
        )
        Instant startTime,

        @Schema(
                description = "Конец диапазона"
        )
        Instant endTime,

        @Schema(
                description = "Количество столбцов",
                example = "10"
        )
        Integer bandAmount
) {
}

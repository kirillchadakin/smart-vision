package pm.smartvision.dto.api;

import io.swagger.v3.oas.annotations.media.Schema;

public record ParamStatusResponse(
        @Schema(
                description = "Статус состояния ошибка по данному параметру (метрике). " +
                        "Значение false означает, что ошибок не было, все в норме, " +
                        "значение true означает, что по параметру (метрике) произошла ошибка.",
                example = "false"
        )
        Boolean status
) {
}

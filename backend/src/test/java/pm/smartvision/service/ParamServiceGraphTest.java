package pm.smartvision.service;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.context.annotation.ComponentScan;
import pm.smartvision.model.ParamValue;
import pm.smartvision.repository.ParamRepositoryTestSupport;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

@DataR2dbcTest
@ComponentScan(basePackages = {"pm.smartvision.repository", "pm.smartvision.service"})
@Disabled
@Log4j2
class ParamServiceGraphTest extends ParamRepositoryTestSupport {

    private static final Instant FROM = Instant.from(DateTimeFormatter.ISO_INSTANT.parse("2024-03-09T15:20:00Z"));
    private static final Instant TO = Instant.from(DateTimeFormatter.ISO_INSTANT.parse("2024-03-10T15:20:00Z"));

    @Autowired
    private ParamService paramService;

    @Test
    public void hist() {
        insert("""
                insert into params (id, name, threshold, "depth", pattern, collector_url) values
                    (1, '404', 20, 60, '.*404.*', 'localhost:123');
                    """);
        Stream.of(
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(10)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(100)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(200)), 5),


                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(626)), 20),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(676)), 5),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(996)), 30),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1167)), 15),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1222)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1272)), 5),

                ParamValue.create(1L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(1L, TO.plus(Duration.ofMinutes(100)), 10)
        ).forEach(paramValue -> paramRepository.addParamValue(paramValue)
                .as(StepVerifier::create)
                .verifyComplete()
        );
        int bandAmount = 5;
        Long paramId = 1L;
        int bandWidthInMinutes = 288;

        paramService.histGraph(paramId, bandAmount, FROM, TO)
                .log()
                .as(StepVerifier::create)
                .expectNext(Map.of(
                                histKey(
                                        FROM,
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes))
                                ), 15L,
                                histKey(
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes)),
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 2))
                                ), 0L,
                                histKey(
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 2)),
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 3))
                                ), 25L,
                                histKey(
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 3)),
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 4))
                                ), 30L,
                                histKey(
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 4)),
                                        FROM.plus(Duration.ofMinutes(bandWidthInMinutes * 5))
                                ), 25L
                        )
                )
                .verifyComplete();
    }

    private String histKey(Instant from, Instant to) {
        return String.format("%s,%s", from, to);
    }

    @Test
    public void circle() {
        insert("""
                insert into params (id, name, threshold, "depth", pattern, collector_url) values
                    (1, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (2, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (3, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (4, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (5, '404', 20, 60, '.*404.*', 'localhost:123');
                    """);

        Stream.of(
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(10)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(100)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(200)), 5),

                ParamValue.create(1L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(1L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(2L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(2L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(2L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(2L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(3L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(3L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(3L, FROM.plus(Duration.ofMinutes(996)), 30),

                ParamValue.create(3L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(3L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(4L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(4L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(4L, FROM.plus(Duration.ofMinutes(1167)), 15),
                ParamValue.create(4L, FROM.plus(Duration.ofMinutes(1222)), 5),
                ParamValue.create(4L, FROM.plus(Duration.ofMinutes(1272)), 5),

                ParamValue.create(4L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(4L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(5L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(5L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(5L, FROM.plus(Duration.ofMinutes(626)), 20),
                ParamValue.create(5L, FROM.plus(Duration.ofMinutes(676)), 5),

                ParamValue.create(5L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(5L, TO.plus(Duration.ofMinutes(100)), 10)
        ).forEach(paramValue -> paramRepository.addParamValue(paramValue)
                .as(StepVerifier::create)
                .verifyComplete()
        );

        paramService.circleGraph(List.of(1L, 2L, 3L, 4L), FROM, TO)
                .log()
                .as(StepVerifier::create)
                .expectNext(Map.of(
                                1L, 15L,
                                2L, 0L,
                                3L, 30L,
                                4L, 25L
                        )
                )
                .verifyComplete();
    }
}
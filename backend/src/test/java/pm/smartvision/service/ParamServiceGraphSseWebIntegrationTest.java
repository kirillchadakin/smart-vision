package pm.smartvision.service;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.reactive.function.client.WebClient;
import pm.smartvision.repository.ParamRepositoryTestSupport;

import java.time.Instant;
import java.util.Map;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Disabled
@Log4j2
public class ParamServiceGraphSseWebIntegrationTest extends ParamRepositoryTestSupport {
    @Autowired
    private ParamService paramService;

    @LocalServerPort
    private int port;

    @Test
    public void histPeriodicalSse() throws InterruptedException {
        insert("""
                insert into params (id, name, threshold, "depth", pattern, collector_url) values
                    (1, '404', 20, 60, '.*404.*', 'localhost:123');
                    """);


        ParameterizedTypeReference<ServerSentEvent<Map<String, Long>>> type
                = new ParameterizedTypeReference<>() {
        };

        WebClient client = WebClient.create("http://localhost:%d".formatted(port));
        client.get().uri(uriBuilder -> uriBuilder
                        .path("/api/params/graph/hist/sse")
                        .queryParam("paramId", "1")
                        .queryParam("bandAmount", "10")
                        .queryParam("period", "5")
                        .queryParam("depth", "60")
                        .build())
                .retrieve()
                .bodyToFlux(type)
                .subscribe(
                        content -> log.info("Time: {} - event: name[{}], id [{}], content[{}] ",
                                Instant.now(), content.event(), content.id(), content.data()),
                        error -> log.error("Error receiving SSE", error),
                        () -> log.info("Completed!!!"));

        Thread.sleep(10_000);

    }
}

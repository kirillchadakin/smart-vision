package pm.smartvision.repository;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.context.annotation.ComponentScan;
import pm.smartvision.dto.db.CircleGraphDto;
import pm.smartvision.model.ParamValue;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

@DataR2dbcTest
@ComponentScan(basePackages = "pm.smartvision.repository")
@Disabled
@Log4j2
public class ParamRepositoryGraphTest extends ParamRepositoryTestSupport {
    private static final Instant FROM = Instant.from(DateTimeFormatter.ISO_INSTANT.parse("2024-03-09T15:20:00Z"));
    private static final Instant TO = Instant.from(DateTimeFormatter.ISO_INSTANT.parse("2024-03-10T15:20:00Z"));

    @Test
    public void hist() {
        insert("""
                insert into params (id, name, threshold, "depth", pattern, collector_url) values
                    (1, '404', 20, 60, '.*404.*', 'localhost:123');
                    """);
        Stream.of(
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(10)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(100)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(200)), 5),


                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(626)), 20),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(676)), 5),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(996)), 30),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1167)), 15),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1222)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1272)), 5),

                ParamValue.create(1L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(1L, TO.plus(Duration.ofMinutes(100)), 10)
        ).forEach(paramValue -> paramRepository.addParamValue(paramValue)
                .as(StepVerifier::create)
                .verifyComplete()
        );
        Long paramId = 1L;

        paramRepository.paramValuesBetween(paramId, FROM, TO)
                .log()
                .as(StepVerifier::create)
                .expectNext(
                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(10)), 5),
                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(100)), 5),
                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(200)), 5),


                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(626)), 20),
                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(676)), 5),

                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(996)), 30),

                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1167)), 15),
                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1222)), 5),
                        ParamValue.create(1L, FROM.plus(Duration.ofMinutes(1272)), 5)
                        )
                .verifyComplete();
    }

    @Test
    public void circle() {
        insert("""
                insert into params (id, name, threshold, "depth", pattern, collector_url) values
                    (1, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (2, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (3, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (4, '404', 20, 60, '.*404.*', 'localhost:123'),
                    (5, '404', 20, 60, '.*404.*', 'localhost:123');
                    """);

        Stream.of(
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(1L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(10)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(100)), 5),
                ParamValue.create(1L, FROM.plus(Duration.ofMinutes(200)), 5),

                ParamValue.create(1L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(1L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(2L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(2L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(2L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(2L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(3L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(3L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(3L, FROM.plus(Duration.ofMinutes(996)), 30),

                ParamValue.create(3L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(3L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(4L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(4L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(4L, FROM.plus(Duration.ofMinutes(1167)), 15),
                ParamValue.create(4L, FROM.plus(Duration.ofMinutes(1222)), 5),
                ParamValue.create(4L, FROM.plus(Duration.ofMinutes(1272)), 5),

                ParamValue.create(4L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(4L, TO.plus(Duration.ofMinutes(100)), 10),


                ParamValue.create(5L, FROM.minus(Duration.ofMinutes(277)), 10),
                ParamValue.create(5L, FROM.minus(Duration.ofMinutes(257)), 10),

                ParamValue.create(5L, FROM.plus(Duration.ofMinutes(626)), 20),
                ParamValue.create(5L, FROM.plus(Duration.ofMinutes(676)), 5),

                ParamValue.create(5L, TO.plus(Duration.ofMinutes(10)), 25),
                ParamValue.create(5L, TO.plus(Duration.ofMinutes(100)), 10)
        ).forEach(paramValue -> paramRepository.addParamValue(paramValue)
                .as(StepVerifier::create)
                .verifyComplete()
        );

        paramRepository.circleGraph(List.of(1L, 2L, 3L, 4L), FROM, TO)
                .log()
                .as(StepVerifier::create)
                .expectNext(
                        new CircleGraphDto(1L, 15L),
                        new CircleGraphDto(3L, 30L),
                        new CircleGraphDto(4L, 25L)
                )
                .verifyComplete();
    }
}

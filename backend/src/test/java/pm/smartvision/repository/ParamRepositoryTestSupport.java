package pm.smartvision.repository;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.r2dbc.core.R2dbcEntityTemplate;
import org.springframework.r2dbc.core.DatabaseClient;
import reactor.test.StepVerifier;

/**
 * При запуске каждого теста очищает бд и записывает тестовые данные из запроса {@link #getTestDataSql()}
 * <br>
 * <b>После выполнения теста все данные будут удалены из бд.</b>
 */
public abstract class ParamRepositoryTestSupport {

    @Autowired
    protected ParamRepository paramRepository;

    @Autowired
    protected R2dbcEntityTemplate template;

    @Autowired
    protected DatabaseClient databaseClient;

    @BeforeAll
    static void cleanUpFirst(@Autowired DatabaseClient databaseClient) {
        cleanUp(databaseClient);
    }

    protected String getTestDataSql() {
        return null;
    }

    @BeforeEach
    void setup() {
        String testDataSql = getTestDataSql();
        if (testDataSql == null) {
            return;
        }
        databaseClient.sql(testDataSql)
                .fetch()
                .rowsUpdated()
                .as(StepVerifier::create)
                .verifyComplete();
    }

    private static void cleanUp(DatabaseClient databaseClient) {
        databaseClient.sql("""
                            delete from param_values;
                            delete from params;
                        """)
                .fetch()
                .rowsUpdated()
                .as(StepVerifier::create)
                .expectNextCount(1)
                .verifyComplete();
    }

    @AfterEach
    void cleanUp() {
        cleanUp(databaseClient);
    }

    protected void insert(String sql) {
        databaseClient.sql(sql)
                .fetch()
                .rowsUpdated()
                .as(StepVerifier::create)
                .expectNextCount(1)
                .verifyComplete();
    }
}

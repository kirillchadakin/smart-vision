package pm.smartvision.repository;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.data.r2dbc.DataR2dbcTest;
import org.springframework.context.annotation.ComponentScan;
import pm.smartvision.model.Param;
import pm.smartvision.model.ParamValue;
import reactor.test.StepVerifier;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

import static org.springframework.data.domain.Sort.by;
import static org.springframework.data.relational.core.query.Criteria.where;
import static org.springframework.data.relational.core.query.Query.empty;
import static org.springframework.data.relational.core.query.Query.query;

/**
 * Проверка работы {@link ParamRepositoryImpl}.
 * <br>
 * <b>После выполнения теста все данные будут удалены из бд.</b>
 */
@DataR2dbcTest
@ComponentScan(basePackages = "pm.smartvision.repository")
@Disabled
@Log4j2
class ParamRepositoryImplTest extends ParamRepositoryTestSupport {

    @Override
    protected String getTestDataSql() {
        return """
                insert into params (id, name, threshold, "depth", pattern, collector_url) values
                    (999, '404', 20, 60, '.*404.*', 'localhost:123');
                                
                insert into param_values (param_id, created_at, value) values
                    (999, now(), 1),
                    (999, now() - '2 minutes'::interval, 1),
                    (999, now() - '4 minutes'::interval, 1),
                    (999, now() - '6 minutes'::interval, 1),
                    (999, now() - '8 minutes'::interval, 1);
                    """;
    }

    private Param exampleParam() {
        Param param = new Param();
        param.setId(999L);
        param.setName("404");
        param.setThreshold(20);
        param.setDepth(60);
        param.setPattern(".*404.*");
        param.setCollectorUrl("localhost:123");
        param.setStatus(true);
        return param;
    }

    @Test
    void findAll() {
        paramRepository.findAll()
                .as(StepVerifier::create)
                .expectNext(exampleParam())
                .verifyComplete();
    }

    @Test
    void save() {
        Param param = exampleParam();
        param.setId(1L);

        paramRepository.save(param)
                .as(StepVerifier::create)
                .expectNext(param)
                .verifyComplete();

        template.select(Param.class)
                .matching(empty().sort(by("id").ascending()))
                .all()
                .as(StepVerifier::create)
                .expectNext(param, exampleParam())
                .verifyComplete();
    }

    @Test
    void update() {
        Param param = exampleParam();
        param.setName("UPDATED");

        paramRepository.update(param)
                .as(StepVerifier::create)
                .expectNext(param)
                .verifyComplete();

        template.select(Param.class)
                .matching(empty().sort(by("id").ascending()))
                .all()
                .as(StepVerifier::create)
                .expectNext(param)
                .verifyComplete();
    }

    @Test
    void remove() {
        Param param = exampleParam();

        template.delete(ParamValue.class)
                .matching(query(where("param_id").is(param.getId())))
                .all()
                .as(StepVerifier::create)
                .expectNext(5L)
                .verifyComplete();

        paramRepository.remove(param.getId())
                .as(StepVerifier::create)
                .expectNextMatches(deletedParam -> Objects.equals(deletedParam.getId(), param.getId()))
                .verifyComplete();

        template.select(Param.class).all()
                .as(StepVerifier::create)
                .expectNextCount(0)
                .verifyComplete();
    }

    @Test
    void getStatusById() {
        Param param = exampleParam();
        paramRepository.getStatusById(param.getId())
                .as(StepVerifier::create)
                .expectNext(false)
                .verifyComplete();
    }

    @Test
    void findValues() {
        Param param = exampleParam();
        paramRepository.findValues(param.getId(), Instant.now().minus(Duration.ofHours(1)), Instant.now())
                .log()
                .as(StepVerifier::create)
                .expectNextCount(5)
                .thenConsumeWhile(paramValue -> Objects.equals(param.getId(), paramValue.getParamId()))
                .verifyComplete();
    }

    @Test
    void addParamValue() {
        Param param = exampleParam();
        Instant createdAt = Instant.now().plus(Duration.ofMinutes(10));
        ParamValue paramValue = ParamValue.create(param.getId(), createdAt, 100);

        paramRepository.addParamValue(paramValue)
                .as(StepVerifier::create)
                .verifyComplete();

        template.select(ParamValue.class)
                .matching(query(where("created_at").is(createdAt)))
                .one()
                .as(StepVerifier::create)
                .expectNextMatches(pv -> Objects.equals(paramValue.getParamId(), pv.getParamId())
                        && Objects.equals(paramValue.getValue(), pv.getValue())
                )
                .verifyComplete();
    }

    @Test
    void clearParamValues() {
        paramRepository.clearParamValues(exampleParam().getId())
                .as(StepVerifier::create)
                .verifyComplete();

        template.select(ParamValue.class)
                .all()
                .as(StepVerifier::create)
                .expectNextCount(0)
                .verifyComplete();
    }
}
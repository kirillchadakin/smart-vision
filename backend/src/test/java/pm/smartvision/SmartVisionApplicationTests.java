package pm.smartvision;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.reactive.server.WebTestClient;
import pm.smartvision.model.Param;
import reactor.core.publisher.Mono;

import java.util.ArrayList;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Disabled
class SmartVisionApplicationTests {

	@Autowired
	WebTestClient webClient;

	@BeforeEach
	void loadData() {

	}

	@Test
	void contextLoads() {
		webClient.get().uri("/api/params")
				.exchange()
				.expectStatus().isOk()
				.expectBodyList(Param.class)
				.hasSize(0);

		ArrayList<Param> params = new ArrayList<>();
		{
			Param param = new Param();
			param.setId(1L); // должен проставиться автоматически
			param.setName("P1");
			param.setThreshold(60);
			param.setDepth(60);
			param.setPattern("404");
			param.setCollectorUrl("localhost:123");
			param.setStatus(true);
			params.add(param);
		}
		{
			Param param = new Param();
			param.setId(2L); // должен проставиться автоматически
			param.setName("P2");
			param.setThreshold(70);
			param.setDepth(20);
			param.setPattern("500");
			param.setCollectorUrl("localhost:123");
			param.setStatus(true);
			params.add(param);
		}
		{
			Param param = new Param();
			param.setId(3L); // должен проставиться автоматически
			param.setName("P3");
			param.setThreshold(40);
			param.setDepth(1000);
			param.setPattern("300");
			param.setCollectorUrl("my-collector.col:123");
			param.setStatus(false);
			params.add(param);
		}
		for (Param param : params) {
			webClient.post().uri("/api/params")
					.body(Mono.just(param), Param.class)
					.exchange()
					.expectStatus().isOk();
		}

		webClient.get().uri("/api/params")
				.exchange()
				.expectStatus().isOk()
				.expectBodyList(Param.class)
				.contains(params.toArray(Param[]::new));
	}

}

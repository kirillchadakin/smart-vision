package pm.smartvision.collector;

import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pm.smartvision.collector.dao.MetricDao;

import java.time.Duration;

@SpringBootTest
@Log4j2
class CollectorApplicationTests {

	@Autowired
	private MetricDao metricDao;

	@Test
	void testFileRead() throws InterruptedException {
		Thread.sleep(Duration.ofMinutes(30));
	}

}

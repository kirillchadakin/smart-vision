package pm.smartvision.collector.dao;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import pm.smartvision.collector.dto.db.FileIndexDto;
import reactor.test.StepVerifier;

@SpringBootTest
class FileIndexDaoTest {

    @MockBean
    MetricDao metricDao;

    @Autowired
    FileIndexDao fileIndexDao;

    @Test
    void test() {
        fileIndexDao.save(new FileIndexDto("test.log", 5))
                .as(StepVerifier::create)
                .expectNextCount(1)
                .verifyComplete();
        fileIndexDao.findByFileName("test.log")
                .as(StepVerifier::create)
                .expectNext(new FileIndexDto("test.log", 5))
                .verifyComplete();
    }

}
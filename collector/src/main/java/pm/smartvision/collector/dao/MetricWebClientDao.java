package pm.smartvision.collector.dao;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import pm.smartvision.collector.config.metric.MetricWebClientProperties;
import pm.smartvision.collector.dto.api.ParamValueRequest;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Component
@Log4j2
public class MetricWebClientDao implements MetricDao {
    private final WebClient webClient;

    public MetricWebClientDao(MetricWebClientProperties metricWebClientProperties) {
        this.webClient = WebClient.builder()
                .baseUrl(metricWebClientProperties.getUrl())
                .build();
    }

    public Mono<Void> saveMetric(Long paramId, Integer value) {
        return webClient.post()
                .uri("/api/params/{paramId}/values", paramId)
                .bodyValue(new ParamValueRequest(paramId, Instant.now(), value))
                .retrieve()
                .bodyToMono(String.class)
                .then();
    }

//    public Mono<Void> saveMetric(Long paramId, Integer value) {
//        return Mono.fromCallable(() -> {
//            log.info("goot {}, {}", paramId, value);
//            return null;
//        });
//    }
}

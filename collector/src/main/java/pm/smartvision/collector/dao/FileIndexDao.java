package pm.smartvision.collector.dao;

import pm.smartvision.collector.dto.db.FileIndexDto;
import reactor.core.publisher.Mono;

public interface FileIndexDao {

    Mono<FileIndexDto> findByFileName(String fileName);

    Mono<FileIndexDto> save(FileIndexDto fileIndexDto);
}

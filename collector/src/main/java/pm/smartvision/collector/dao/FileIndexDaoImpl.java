package pm.smartvision.collector.dao;

import jakarta.annotation.PostConstruct;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;
import pm.smartvision.collector.config.metric.MetricsConfig;
import pm.smartvision.collector.dto.db.FileIndexDto;
import reactor.core.publisher.Mono;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

@Component
@Log4j2
public class FileIndexDaoImpl implements FileIndexDao {
    private final MetricsConfig metricsConfig;
    private final Map<String, Long> cachedIndexes = new ConcurrentHashMap<>();

    public FileIndexDaoImpl(MetricsConfig metricsConfig) {
        this.metricsConfig = metricsConfig;
    }

    @PostConstruct
    public void initIndexesFile() throws IOException {
        Path indexPath = metricsConfig.getIndexFile();
        if (Files.notExists(indexPath)) {
            Files.createDirectories(indexPath.toAbsolutePath().getParent());
            Files.createFile(indexPath);
        } else {
            loadIndexes();
        }
    }

    private void loadIndexes() throws IOException {
        try (Stream<String> lines = Files.lines(metricsConfig.getIndexFile())) {
            lines.map(line -> line.split(","))
                    .map(fields -> new FileIndexDto(fields[0], Integer.parseInt(fields[1])))
                    .forEach(fileIndexDto -> cachedIndexes.put(fileIndexDto.fileName(), fileIndexDto.lastLine()));
        }
    }

    @Override
    public Mono<FileIndexDto> findByFileName(String fileName) {
        return Mono.fromCallable(() -> cachedIndexes.get(fileName))
                .map(lastLine -> new FileIndexDto(fileName, lastLine));
    }

    @Override
    public Mono<FileIndexDto> save(FileIndexDto fileIndexDto) {
        return Mono.fromCallable(() -> {
                    cachedIndexes.put(fileIndexDto.fileName(), fileIndexDto.lastLine());
                    writeCachedIndexesToFile();
                    return fileIndexDto;
                });
    }

    private void writeCachedIndexesToFile() {
        try (BufferedWriter writer = Files.newBufferedWriter(metricsConfig.getIndexFile(), StandardOpenOption.TRUNCATE_EXISTING)) {
            for (Map.Entry<String, Long> entry : cachedIndexes.entrySet()) {
                String fileName = entry.getKey();
                Long lastLine = entry.getValue();
                writer.write("%s,%s".formatted(fileName, lastLine));
                writer.newLine();
            }
        } catch (IOException e) {
            log.error("Failed save indexes.", e);
        }
    }
}

package pm.smartvision.collector.dao;

import reactor.core.publisher.Mono;

public interface MetricDao {
    Mono<Void> saveMetric(Long paramId, Integer value);
}

package pm.smartvision.collector.service;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.io.monitor.FileAlterationListenerAdaptor;
import org.apache.commons.io.monitor.FileAlterationMonitor;
import org.apache.commons.io.monitor.FileAlterationObserver;
import org.springframework.stereotype.Service;
import pm.smartvision.collector.config.metric.MetricConfig;
import pm.smartvision.collector.config.metric.MetricsConfig;
import pm.smartvision.collector.dao.FileIndexDao;
import pm.smartvision.collector.dao.MetricDao;
import pm.smartvision.collector.dto.db.FileIndexDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Сохраняет метрики с помощью HTTP запросов к сервису зранения метрик.
 */
@Service
@Log4j2
public class MetricService {
    private final MetricsConfig metricsConfig;
    private final MetricDao metricDao;
    private final FileIndexDao fileIndexDao;
    private final FileAlterationMonitor fileMonitor;


    public MetricService(MetricDao metricDao, MetricsConfig metricsConfig, FileIndexDao fileIndexDao) {
        this.metricDao = metricDao;
        this.metricsConfig = metricsConfig;
        this.fileIndexDao = fileIndexDao;
        this.fileMonitor = new FileAlterationMonitor(metricsConfig.getFileMonitorInterval());
    }

    @PostConstruct
    public void setupMonitor() throws Exception {
        File parentDir = metricsConfig.getParentDirectory().toAbsolutePath().toFile();
        FileAlterationObserver observer = new FileAlterationObserver(parentDir);
        observer.addListener(new FileListener());
        fileMonitor.addObserver(observer);
        fileMonitor.start();
    }

    @PreDestroy
    public void stopMonitor() throws Exception {
        fileMonitor.stop();
    }

    private class FileListener extends FileAlterationListenerAdaptor {
        @Override
        public void onFileChange(File file) {
            Flux.fromIterable(metricsConfig.getConfigs())
                    .filter(configFile -> configFile.getFile()
                            .normalize()
                            .toAbsolutePath()
                            .equals(file.toPath().
                                    normalize()
                                    .toAbsolutePath()
                            ))
                    .flatMap(this::readAndSendUpdates)
                    .blockLast();
        }

        private Mono<Void> readAndSendUpdates(MetricConfig metricConfig) {
            String fileName = metricConfig.getFile().toString();
            return fileIndexDao.findByFileName(fileName)
                    .switchIfEmpty(fileIndexDao.save(new FileIndexDto(fileName, 0)))
                    .flatMapMany(fileIndexDto -> {
                        Flux<String> lines = read(fileIndexDto, metricConfig).publish().refCount(2);
                        lines.count().flatMap(lineCount -> fileIndexDao.save(
                                new FileIndexDto(fileName, fileIndexDto.lastLine() + lineCount))
                        ).subscribe();
                        return lines;
                    })
                    .filter(line -> line.matches(metricConfig.getPattern()))
                    .flatMap(line -> metricDao.saveMetric(metricConfig.getParamId(), 1))
                    .then();
        }

        private Flux<String> read(FileIndexDto fileIndexDto, MetricConfig metricConfig) {
            Path file = metricsConfig.getParentDirectory().toAbsolutePath().resolve(metricConfig.getFile());
            return Flux.using(() -> Files.lines(file), Flux::fromStream)
                    .skip(fileIndexDto.lastLine());
        }

    }
}

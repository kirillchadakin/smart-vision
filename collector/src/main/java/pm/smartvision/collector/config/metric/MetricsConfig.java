package pm.smartvision.collector.config.metric;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@ConfigurationProperties(prefix = "metric")
@Configuration
public class MetricsConfig {
    long fileMonitorInterval = 1000;
    // default is working dir
    Path parentDirectory = Paths.get("");
    Path indexFile = Paths.get("indexes.csv");
    List<MetricConfig> configs = new ArrayList<>();
}

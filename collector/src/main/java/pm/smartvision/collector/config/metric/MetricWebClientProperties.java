package pm.smartvision.collector.config.metric;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "metric.client")
@Configuration
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MetricWebClientProperties {
    String url;
}

package pm.smartvision.collector.config.metric;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import java.nio.file.Path;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MetricConfig {
    Path file;
    Long paramId;
    String pattern = ".*";
}

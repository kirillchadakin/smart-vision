package pm.smartvision.collector.dto.api;

import java.time.Instant;

public record ParamValueRequest (
    Long paramId,
    Instant instant,
    Integer value
) {
}
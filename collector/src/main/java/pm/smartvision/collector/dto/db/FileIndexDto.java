package pm.smartvision.collector.dto.db;

public record FileIndexDto(
        String fileName,
        long lastLine
) {
}

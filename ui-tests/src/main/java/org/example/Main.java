import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.time.Duration;
//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\User\\Desktop\\driver\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.get("http://212.22.94.121:8080/");


        WebElement startButton = driver.findElement(By.className("start-button"));
        startButton.click();


        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        WebElement loginButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("login")));


        loginButton.click();

        WebElement nameInput = driver.findElement(By.xpath("//input[@placeholder='Введите название...']"));
        nameInput.sendKeys("Название параметра");


        WebElement urlInput = driver.findElement(By.xpath("//input[@placeholder='Введите url...']"));
        urlInput.sendKeys("http://example.com");


        WebElement errorsInput = driver.findElement(By.xpath("//input[@placeholder='Максимум ошибок...']"));
        errorsInput.sendKeys("10");


        WebElement createButton = driver.findElement(By.xpath("//div[@class='big-button']"));
        createButton.click();
    }
}

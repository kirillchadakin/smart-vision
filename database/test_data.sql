begin;

insert into params (id, name, threshold, "depth", pattern, collector_url) values
    (999, '404', 20, 60, '.*404.*', 'localhost:123');

insert into param_values (param_id, created_at, value) values
    (999, now(), 1),
    (999, now() - '2 minutes'::interval, 1),
    (999, now() - '4 minutes'::interval, 1),
    (999, now() - '6 minutes'::interval, 1),
    (999, now() - '8 minutes'::interval, 1);

commit;
begin;

create table if not exists params (
    id serial primary key,
    name varchar(255) not null,
    threshold int not null,
    depth int not null,
    pattern varchar(255) not null,
    collector_url varchar(255) not null,
    status boolean not null default TRUE
);

create table if not exists param_values (
    param_id bigint references params,
    created_at timestamptz not null,
    value int not null,
    primary key (param_id, created_at)
);

commit;